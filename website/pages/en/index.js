/**
 * Copyright (c) 2017-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

const React = require("react");

const CompLibrary = require("../../core/CompLibrary.js");

const MarkdownBlock = CompLibrary.MarkdownBlock; /* Used to read markdown */
const Container = CompLibrary.Container;
const GridBlock = CompLibrary.GridBlock;

const siteConfig = require(`${process.cwd()}/siteConfig.js`);

function imgUrl(img) {
  return `${siteConfig.baseUrl}img/${img}`;
}

function docUrl(doc, language) {
  return `${siteConfig.baseUrl}docs/${language ? `${language}/` : ""}${doc}`;
}

function pageUrl(page, language) {
  return siteConfig.baseUrl + (language ? `${language}/` : "") + page;
}

class Button extends React.Component {
  render() {
    return (
      <div className="pluginWrapper buttonWrapper">
        <a className="button" href={this.props.href} target={this.props.target}>
          {this.props.children}
        </a>
      </div>
    );
  }
}

Button.defaultProps = {
  target: "_self"
};

const SplashContainer = props => (
  <div className="homeContainer">
    <div className="homeSplashFade">
      <div className="wrapper homeWrapper">{props.children}</div>
    </div>
  </div>
);

const Logo = props => (
  <div className="projectLogo">
    <img src={props.img_src} alt="Project Logo" />
  </div>
);

const ProjectTitle = () => (
  <h2 className="projectTitle">
    {siteConfig.title}
    <small>{siteConfig.tagline}</small>
  </h2>
);

const PromoSection = props => (
  <div className="section promoSection">
    <div className="promoRow">
      <div className="pluginRowBlock">{props.children}</div>
    </div>
  </div>
);

class HomeSplash extends React.Component {
  render() {
    const language = this.props.language || "";
    return (
      <SplashContainer>
        <Logo img_src={imgUrl("dove.svg")} />
        <div className="inner">
          <ProjectTitle />
          <PromoSection>
            <Button href="#try">Grupos</Button>
          </PromoSection>
        </div>
      </SplashContainer>
    );
  }
}

const Block = props => (
  <Container
    padding={["bottom", "top"]}
    id={props.id}
    background={props.background}
  >
    <GridBlock align="center" contents={props.children} layout={props.layout} />
  </Container>
);

const Features = () => (
  <Block layout="fourColumn">
    {[
      {
        content: `O front-end é responsável por coletar a entrada em várias formas do usuário e
         processá-la para adequá-la a uma especificação útil para o back-end.`,
        image: imgUrl("html.svg"),
        imageAlign: "top",
        title: "Frontend"
      },
      {
        content: `O banck-end da plataforma de desaparecido é uma api desenvolvida em Flask,
         que armazena e envia notificações`,
        image: imgUrl("api.svg"),
        imageAlign: "top",
        title: "Api"
      }
    ]}
  </Block>
);

const FeatureCallout = () => (
  <div
    className="productShowcaseSection paddingBottom"
    style={{ textAlign: "center" }}
  >
    <h2>Funcionalidades</h2>

    <p>Geolocalização dos desaparecidos</p>
    <p>Notificação de avistado através do Telegram</p>
    <p>Notificação de avistado através de Email</p>
    <p>Mapa de desaparecimento</p>
    <p>Dashboard de desaparecidos</p>

    <p>Para conferir o que mais desejamos desenvolver acesse <a href="https://trello.com/b/tYk0GQr1/aplicativo">Trello Desaparecidos.me</a></p>
  </div>
);

const LearnHow = () => (
  <Block background="light">
    {[
      {
        content: `A desaparecidos.me é um projeto social que não visa lucro, foi desenvolvido para devolver pessoas desaparecidas para seus familiares, e esta sendo mantido através de iniciativa própria.`,
        image: imgUrl("dove.svg"),
        imageAlign: "right",
        title: "Sobre o projeto"
      }
    ]}
  </Block>
);

const TryOut = () => (
  <Block id="try">
    {[
      {
        content: `<ul style='text-align: left;'><li><a href="https://www.facebook.com/groups/599367403411145/?ref=group_browse_new">Desaparecidos</a></li>
        <li><a href="https://www.facebook.com/groups/323441258044584/?ref=group_browse_new">Procura-se por pessoas desaparecidas</a></li>
        <li><a href="https://www.facebook.com/groups/782813425261356/?ref=group_browse_new">Pessoas Desaparecidas RS</a></li>
        <li><a href="https://www.facebook.com/groups/1501902359886408/?ref=group_browse_new">Pessoas desaparecidas São Paulo</a></li>
        <li><a href="https://www.facebook.com/groups/Desaparecido.sem.Osasco/?ref=group_browse_new">Desaparecidos em Osasco</a></li>
        <li><a href="https://www.facebook.com/groups/1930396807227213/?ref=group_browse_new">Divulgando Pessoas Desaparecidas e Desencontradas.</a></li>
        <li><a href="https://www.facebook.com/groups/desaparecidaseperdidas/?ref=group_browse_new">PESSOAS DESAPARECIDAS E PERDIDAS</a></li>
        <li><a href="https://www.facebook.com/groups/grsomgomes/">Ajude a Encontrar Pessoas Desaparecidas</a></li>`,
        image: imgUrl("group.svg"),
        imageAlign: "left",
        title: "Grupos no facebook"
      }
    ]}
  </Block>
);

class Index extends React.Component {
  render() {
    const language = this.props.language || "";

    return (
      <div>
        <HomeSplash language={language} />
        <div className="mainContainer">
          <Features />
          <FeatureCallout />
          <LearnHow />
          <TryOut />
        </div>
      </div>
    );
  }
}

module.exports = Index;
