---
id: doc2
title: api desaparecidos
---

## Api

https://gitlab.com/desaparecidos.me/api-desaparecidos

### Objetivo

Camada responsavel por armazenar distribuir e notificar o usuário

### Repository

https://gitlab.com/desaparecidos.me/api-desaparecidos.git


### Arquitetura

Api restful, integrada ao mongodb e redis, com fila de processamento assyncrono com celery.

![Arquitetura](../img/integration.svg)

### Covertura e avaliação

- [![Codacy Badge](https://api.codacy.com/project/badge/Grade/a932cba8adfe455083a366b1fb6b5657)](https://www.codacy.com?utm_source=gitlab.com&amp;utm_medium=referral&amp;utm_content=lcosta.sp.br/api-desaparecidos&amp;utm_campaign=Badge_Grade)
- [![Codacy Badge](https://api.codacy.com/project/badge/Coverage/a932cba8adfe455083a366b1fb6b5657)](https://www.codacy.com?utm_source=gitlab.com&utm_medium=referral&utm_content=lcosta.sp.br/api-desaparecidos&utm_campaign=Badge_Coverage)

### Requirements

```bash 
python 3.6
virtualenv
mongodb
redis
celery
```

### Set Up

```bash
$ echo "Install virtualenv"
$ pip install virtualenv
$ echo "Create virtualenv"
$ virtualenv venv
$ source venv/bin/activate
$ echo "Install requirements"
$ pip install -r requirements.txt
```

### Environments 

```bash
$ export TELEGRAM_KEY="640445579:AAEUMQZTw8yioq_SUR1Zq1fcMpT1indTSe4"
$ export TELEGRAM_CHAT_ID="@desaparecidosmestg"
$ export MONGODB_URI=mongodb://localhost:27017/api-desaparecidos
$ export REDIS_URL=redis://localhost:6379/0
$ export BASIC_AUTH_USERNAME=""
$ export BASIC_AUTH_PASSWORD=""
```

### Docker run dependencies

```bash
$ docker run --name desaparecidos-redis -p 6379:6379 -d redis
$ docker run --name desaparecidos-mongo -p 27017:27017 -d mongo
```

### Celery run workers

```bash
$ celery worker -A app.celery --loglevel=info
```

### Run app

```bash
$ gunicorn app:app
```
