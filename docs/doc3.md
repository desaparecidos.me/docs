---
id: doc3
title: Versões da plataforma
---

## v1

![Arquitetura](../img/versions/v1.svg)

## v2 current

![Arquitetura](../img/versions/v2.svg)

## v3

![Arquitetura](../img/versions/v3.svg)